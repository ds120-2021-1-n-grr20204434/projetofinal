<?php
require "db_functions.php";
require "force_authenticate.php";

$error = false;
$success = false;
$time = date("Y.m.d");

date_default_timezone_set("America/Sao_Paulo");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["peso"]) && isset($_POST["altura"])) {

    $conn = connect_db();

    $peso = mysqli_real_escape_string($conn,$_POST["peso"]);
    $altura = mysqli_real_escape_string($conn,$_POST["altura"]);
    $imc = mysqli_real_escape_string($conn,$_POST["imc"]);
    $created_at = mysqli_real_escape_string($conn,$time);
    $name_imc = $_SESSION['user_id'];

    $sql = "INSERT INTO $table_imc
            (name_imc, peso, altura, imc, created_at) VALUES
            ('$name_imc', '$peso', '$altura', '$imc', '$created_at');";

    if(mysqli_query($conn, $sql)){
      $success = true;
      echo '<div class="b">';
      echo "<br>Informações salvas com sucesso<br>";
      echo '</div>';
    }
    else {
      $error_msg = mysqli_error($conn);
      $error = true;
      echo '<div class="b">';
      echo "<br>As informações não foram salvas!<br>";
      echo '</div>';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    <title>Calcular IMC</title>
    <!-- para usar jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- para linkar o main.js -->
    <script type="text/javascript" src="main.js"></script>
  </head>
  <body>
    <h1>Coloque os dados</h1>
    <div class="quadrado2">
      <form action="imccalc.php" method="post">
        <label for="peso">Peso: </label>
        <input type="number" step='0.01' name="peso" value="<?php echo $peso; ?>" required onchange="atualizar_imc()"><br>

        <label for="altura">Altura: </label>
        <input type="number" step='0.01' name="altura" value="<?php echo $altura; ?>" required onchange="atualizar_imc()"><br>

        <label for="imc">IMC: </label>
        <input type="number" step='0.01' name="imc" value="<?php echo $imc; ?>" required readonly><br>

        <br><input type="submit" class="a" name="submit" value="Enviar">
      </form>
      <ul>
        <li><a href="index.php">Voltar</a></li>
      </ul>
    </div>
  </body>
</html>
