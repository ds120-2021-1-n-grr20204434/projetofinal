<?php
//para ser uma página diferente para o usuário logado
  require "authenticate.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    <title>WEB APP Imc, Peso e altura</title>
  </head>
  <body>
    <h1>Calculadora de IMC</h1>
    <div class="telaprincipal">Bem-vindo
      <?php
      if ($login) {
        echo " $user_name!";
      }
      else {
        echo "!";
      }
      ?>  <br><br>Menu
    </div>
    <div class="quadrado2">
      <ul>
        <?php if ($login): ?>
          <li><a href="logout.php">Logout</a></li>
        <?php else: ?>
          <li><a href="login.php">Logar</a></li>
          <li><a href="register.php">Registrar</a></li>
        <?php endif; ?>
      </ul>
      <?php if ($login): ?>
        <ul>
          <li><a href="imccalc.php">Calcular IMC</a></li>
          <li><a href="tabelaimc.php">Tabela de IMC</a></li>
        </ul>
      <?php endif; ?>
    </div>
    <div class="textinho">
      IMC é a sigla para Índice de Massa Corporal, é um cálculo
      para avaliar se a pessoa está dentro de seu peso ideal
      em relação à altura. Assim, com o valor do resultado de IMC,
      a pessoa pode saber se está dentro do peso ideal, acima ou abaixo do
      peso desejado.
    </div>
  </body>
</html>
