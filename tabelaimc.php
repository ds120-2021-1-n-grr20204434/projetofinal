<?php
require 'db_credentials.php';
require "force_authenticate.php";

  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
  // $sql = "SELECT name_imc, peso, altura, imc, created_at FROM $table_imc";
  $sql = "SELECT users.name, imc.peso, imc.altura, imc.imc, imc.created_at FROM $table_imc AS imc INNER JOIN $table_users AS users ON imc.name_imc = users.id AND users.id = $user_id";
  $result = mysqli_query($conn, $sql);
  $tabeladeimc = "";

  if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
      $tabeladeimc .="Nome: " . $row["name"] . " " . $row["peso"]." KG" ." | ". $row["altura"] . " M | IMC: " . $row["imc"] . " | " . $row["created_at"] ."<br>";
    }
  } else {
    echo "";
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="estilo.css">
    <title>Tabela de IMC</title>
  </head>
  <body>
    <h1>Tabela de IMC</h1>
    <div class="tabelinhaimc">
      <?php echo $tabeladeimc ?>
    </div>
    <div class="quadrado2">
      <ul>
        <br><li><a href="index.php">Voltar</a></li><br>
      </ul>
    </div>
  </body>
</html>
