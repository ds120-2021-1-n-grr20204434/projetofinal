<?php
require "db_functions.php";

$error = false;
$success = false;
$name = $email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirm_password"])) {

    $conn = connect_db();

    $name = mysqli_real_escape_string($conn,$_POST["name"]);
    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $confirm_password = mysqli_real_escape_string($conn,$_POST["confirm_password"]);

    if ($password == $confirm_password) {
      $password = md5($password);

      if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $sql = "INSERT INTO $table_users
                (name, email, password) VALUES
                ('$name', '$email', '$password');";

        if(mysqli_query($conn, $sql)){
          $success = true;
        }
        else {
          $error_msg = mysqli_error($conn);
          $error = true;
        }
      }
      else {
        $error_msg = "E-mail inválido";
        $error = true;
      }
    }
    else {
      $error_msg = "Senha não confere com a confirmação.";
      $error = true;
    }
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>[WEB 1] Exemplo Sistema de Login - Registro</title>
  <link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
<h1>Dados para registro</h1>
<div class="b">
<?php if ($success): ?>
  Usuário criado com sucesso!
  Seguir para <a href="login.php">Login</a>
<?php endif;
  if ($error):
  echo $error_msg;
  endif; ?>
</div>
  <div class="quadrado2">
    <form action="register.php" method="post">
      <div class="formulario"
      <label for="name">Nome: </label>
      <input type="text" name="name" value="<?php echo $name; ?>" required><br>

      <label for="email">Email: </label>
      <input type="text" name="email" value="<?php echo $email; ?>" required><br>

      <label for="password">Senha: </label>
      <input type="password" name="password" value="" required><br>

      <label for="confirm_password">Confirmação da Senha: </label>
      <input type="password" name="confirm_password" value="" required><br><br>
      </div>
      <input type="submit" class="submit" name="submit" value="Registrar">
    </form>
    <ul>
      <li><a href="index.php">Voltar</a></li>
    </ul>
  </div>
</body>
</html>
