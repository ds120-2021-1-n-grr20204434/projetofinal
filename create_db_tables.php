<?php
require 'db_credentials.php';

// Create connection
$conn = mysqli_connect($servername, $username, $db_password);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create database
$sql = "CREATE DATABASE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database created successfully<br>";
} else {
    echo "<br>Error creating database: " . mysqli_error($conn);
}

// Choose database
$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database changed successfully<br>";
} else {
    echo "<br>Error changing database: " . mysqli_error($conn);
}

// sql to create table, é um array que cada string é um comando
$sql = ["CREATE TABLE $table_users (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(128) NOT NULL,
  created_at DATETIME,
  updated_at DATETIME,
  last_login_at DATETIME,
  last_logout_at DATETIME,
  UNIQUE (email)
)",
"CREATE TABLE $table_imc (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name_imc INT(6) UNSIGNED,
  peso VARCHAR(100) NOT NULL,
  altura VARCHAR(100) NOT NULL,
  imc VARCHAR(100) NOT NULL,
  created_at DATE NOT NULL
)",
"ALTER TABLE imc ADD FOREIGN KEY (name_imc) REFERENCES Users(id)"
];
// executa todos os comandos do array
foreach ($sql as $sql) {
  if (mysqli_query($conn, $sql)) {
      echo "<br>Table created successfully<br>";
  } else {
      echo "<br>Error creating database: " . mysqli_error($conn);
  }
}


mysqli_close($conn)
?>
