<?php
require "db_functions.php";
require "authenticate.php";

$error = false;
$password = $email = "";

if (!$login && $_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["email"]) && isset($_POST["password"])) {

    $conn = connect_db();

    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $password = md5($password);

    $sql = "SELECT id,name,email,password FROM $table_users
            WHERE email = '$email';";

    $result = mysqli_query($conn, $sql);
    if($result){
      if (mysqli_num_rows($result) > 0) {
        $user = mysqli_fetch_assoc($result);

        if ($user["password"] == $password) {

          $_SESSION["user_id"] = $user["id"];
          $_SESSION["user_name"] = $user["name"];
          $_SESSION["user_email"] = $user["email"];

          header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "index.php");
          exit();
        }
        else {
          $error_msg = "Usuário ou senha incorretos";
          $error = true;
        }
      }
      else{
        $error_msg = "Usuário ou senha incorretos";
        $error = true;
      }
    }
    else {
      $error_msg = mysqli_error($conn);
      $error = true;
    }
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="estilo.css">
  <title>[WEB 1] Exemplo Sistema de Login - Registro</title>
</head>
<body>
<h1>Login</h1>
<div class="b">
  <?php if ($login): ?>
      Você já está logado!
    </body>
    </html>
    <?php exit(); ?>
  <?php endif; ?>

  <?php if ($error): ?>
    <?php echo $error_msg; ?>
  <?php endif; ?>
</div>
<div class="quadrado2">
  <form action="login.php" method="post">
    <label for="email">Email: </label>
    <input type="text" name="email" value="<?php echo $email; ?>" required><br>

    <label for="password">Senha: </label>
    <input type="password" name="password" value="" required><br><br>

    <input type="submit" class="a" name="submit" value="Logar">
  </form>
  <ul>
    <li><a href="index.php">Voltar</a></li>
  </ul>
</div>
</body>
</html>
